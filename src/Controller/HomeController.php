<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    #[Route('/home')]
    public function home()
    {
        return new Response(" Bienvenue sur la page d'accueil ! ");
    }

    #[Route('/article/{articleId}',methods: ['GET'])]
    public function show($articleId)
    {
        return new Response(" Voici le contenu de l'article avec l'ID $articleId ");
    }
}
